package com.gitlab.lauropereira.carfixondemand.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gitlab.lauropereira.carfixondemand.R;
import com.gitlab.lauropereira.carfixondemand.model.Customer;

import java.util.List;

public class CustomerAdapter extends BaseAdapter {

    private Context context;
    private List<Customer> list;

    public CustomerAdapter(){}
    public CustomerAdapter(Context context, List<Customer> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return new Long(list.get(position).getCpf().replaceAll("[^\\d]", ""));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.customer_item, null);

        TextView nome = (TextView) layout.findViewById(R.id.customer_name);
        TextView email = (TextView) layout.findViewById(R.id.customer_email);
        TextView phone = (TextView) layout.findViewById(R.id.customer_phone);

        final Customer customer = (Customer) this.getItem(position);
        nome.setText(customer.getName().trim());
        email.setText(customer.getEmail().trim());
        phone.setText("(" + customer.getPhone().substring(0,1) + ") " + customer.getPhone().substring(2, 5) + "-" + customer.getPhone().substring(6));

        return layout;
    }
}
