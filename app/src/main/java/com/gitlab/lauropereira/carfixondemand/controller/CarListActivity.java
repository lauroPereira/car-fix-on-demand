package com.gitlab.lauropereira.carfixondemand.controller;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gitlab.lauropereira.carfixondemand.R;
import com.gitlab.lauropereira.carfixondemand.adapter.CarAdapter;
import com.gitlab.lauropereira.carfixondemand.model.Car;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import util.WebServiceUtil;

public class CarListActivity extends AppCompatActivity {

    private ListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CarAddActivity.class);
                startActivity(intent);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String url = "https://car-fix-on-demand.firebaseio.com/cars";
        new WebService().execute(url);
    }

    private void geraLista(List<Car> lista) {
        list = (ListView) findViewById(R.id.customers_list);

        CarAdapter ad = new CarAdapter(getApplicationContext(), lista);

        list.setAdapter(ad);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(), "teste leitura de 1 item", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private class WebService extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
                return WebServiceUtil.getContentAsString(urls[0]);
            } catch (IOException e) {
                Log.e("Exception", e.toString());
                return "Erro ao processar os dados da API";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Gson parser = new Gson();
            Type listType = new TypeToken<ArrayList<Car>>() {
            }.getType();

            try {
                List<Car> retorno = new Gson().fromJson(result, listType);
                geraLista(retorno);
            } catch (Exception exc) {
                Log.e("Lista Clientes: ", "Falha ao processar retorno: " + exc.getMessage());
            }
        }
    }

}
