package com.gitlab.lauropereira.carfixondemand.model;

import android.provider.ContactsContract;

import java.io.Serializable;
import java.util.ArrayList;

public class Customer{
    private String name;
    private String cpf;
	private String phone;
	private String email;
	private ArrayList<Car> cars;

    public Customer() {}
    public Customer(String name, String cpf, String phone, String email, ArrayList<Car> cars) {
        this.name = name;
        this.cpf = cpf;
        this.phone = phone;
        this.email = email;
        this.cars = cars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Car> getCars() {
        return cars;
    }

    public void setCars(ArrayList<Car> cars) {
        this.cars = cars;
    }
}
