package com.gitlab.lauropereira.carfixondemand.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.gitlab.lauropereira.carfixondemand.MainActivity;
import com.gitlab.lauropereira.carfixondemand.R;
import com.gitlab.lauropereira.carfixondemand.model.Customer;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

public class CustomerAddActivity extends AppCompatActivity {

    private static final String TAG = "CustomerAddActivity";
    private static final String KEY = "customers";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String msg;
                if(saveCustomer())
                    msg = "Cliente salvo com sucesso!";
                else
                    msg = "Falha ao salvar cliente";
                Snackbar.make(view, msg, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private boolean saveCustomer(){

        Customer customer = new Customer();

        EditText name =  (EditText) findViewById(R.id.st_name);
        customer.setName(name.getText().toString().trim());

        EditText cpf =  (EditText) findViewById(R.id.st_cpf);
        customer.setCpf(cpf.getText().toString().replaceAll("[^\\d]",""));

        EditText phone=  (EditText) findViewById(R.id.st_phone);
        customer.setPhone(phone.getText().toString());

        EditText email=  (EditText) findViewById(R.id.st_mail);
        customer.setEmail(email.getText().toString());

        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        db.child(KEY).child(customer.getCpf()).setValue(customer);

        return true;

    }

}
