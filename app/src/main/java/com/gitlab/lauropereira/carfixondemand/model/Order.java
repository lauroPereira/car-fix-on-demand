package com.gitlab.lauropereira.carfixondemand.model;

import java.util.ArrayList;
import java.util.Date;

public class Order {
    private String id;
    private String services;
    private ArrayList<String> pieces;
    private Car car;
    private Date init_date;
    private Date final_date;
    private String status;
    private float value;

    public Order() {}
    public Order(String id, String services, ArrayList<String> pieces, Car car, Date init_date, Date final_date, String status, float value) {
        this.id = id;
        this.services = services;
        this.pieces = pieces;
        this.car = car;
        this.init_date = init_date;
        this.final_date = final_date;
        this.status = status;
        this.value = value;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public ArrayList<String> getPieces() {
        return pieces;
    }

    public void setPieces(ArrayList<String> pieces) {
        this.pieces = pieces;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Date getInit_date() {
        return init_date;
    }

    public void setInit_date(Date init_date) {
        this.init_date = init_date;
    }

    public Date getFinal_date() {
        return final_date;
    }

    public void setFinal_date(Date final_date) {
        this.final_date = final_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
