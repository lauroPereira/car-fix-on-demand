package com.gitlab.lauropereira.carfixondemand.model;

public class Car {
    private String type; //carros, motos ou caminhoes
    private String brand;
    private String model;
    private String color;
    private String plate;
    private Customer owner;

    public Car() {}
    public Car(String type, String brand, String model, String color, String plate, Customer owner) {
        this.type = type;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.plate = plate;
        this.owner = owner;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }
}
