package com.gitlab.lauropereira.carfixondemand.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gitlab.lauropereira.carfixondemand.R;
import com.gitlab.lauropereira.carfixondemand.model.Car;

import java.util.List;

public class CarAdapter extends BaseAdapter {

    private Context context;
    private List<Car> list;

    public CarAdapter(){}
    public CarAdapter(Context context, List<Car> list){
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.car_item, null);

        TextView plate = (TextView) layout.findViewById(R.id.st_car_plate);
        TextView brand = (TextView) layout.findViewById(R.id.car_brand);
        TextView model = (TextView) layout.findViewById(R.id.st_car_model);
        TextView color = (TextView) layout.findViewById(R.id.st_car_color);

        final Car car = (Car) this.getItem(position);
        plate.setText(car.getPlate().trim());
        brand.setText(car.getBrand().trim());
        model.setText(car.getModel());
        color.setText(car.getColor());

        return layout;
    }
}
