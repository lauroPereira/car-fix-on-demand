package com.gitlab.lauropereira.carfixondemand.controller;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gitlab.lauropereira.carfixondemand.MainActivity;
import com.gitlab.lauropereira.carfixondemand.R;
import com.gitlab.lauropereira.carfixondemand.model.Car;
import com.gitlab.lauropereira.carfixondemand.model.Customer;
import com.gitlab.lauropereira.carfixondemand.model.Order;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.UUID;

import static android.provider.Contacts.SettingsColumns.KEY;

public class OrderAddActivity extends AppCompatActivity {

    File photoPath;
    private String photoName;
    static final String KEY = "order";
    static final String TAG = "OrderAddActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveOrder();
                Toast.makeText(OrderAddActivity.this, "Ordem de serviço salva com sucesso", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        Button bt = (Button) findViewById(R.id.btn_photo);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photo = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(photo,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode,
                                    Intent intentReturn){
        this.photoPath = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        this.photoName = UUID.randomUUID() + ".png";


        if(intentReturn != null){
            Bundle bundle = intentReturn.getExtras();
            if(bundle != null){
                Bitmap img = (Bitmap) bundle.get("data");

                photoSave(img, this.photoPath, this.photoName);

                ImageView iv = (ImageView) findViewById(R.id.order_photo);
                iv.setImageBitmap(img);
            }
        }
    }

    private void saveOrder() {
        Car car = new Car();

        EditText plate =  (EditText) findViewById(R.id.st_car_plate);
        car.setPlate(plate.getText().toString().toUpperCase().replaceAll("[^(\\d|[A-Z])]",""));

        Customer owner =  new Customer();
        EditText cpfOwner=  (EditText) findViewById(R.id.st_customer_cpf);
        owner.setCpf(cpfOwner.getText().toString().replaceAll("[^\\d]",""));
        car.setOwner(owner);

        Order order =  new Order();
        order.setId(this.photoName);
        EditText services =  (EditText) findViewById(R.id.st_service_desc);
        order.setServices(services.getText().toString());
        order.setStatus("AGUARDANDO");
        order.setInit_date(new Date());
        order.setCar(car);

        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        db.child(KEY).child(car.getPlate()).setValue(car);
    }

    private String photoSave(Bitmap bitmapImage, File directory, String filename){

        File mypath = new File(directory, filename);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }
}
