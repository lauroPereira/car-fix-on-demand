package com.gitlab.lauropereira.carfixondemand.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gitlab.lauropereira.carfixondemand.MainActivity;
import com.gitlab.lauropereira.carfixondemand.R;
import com.gitlab.lauropereira.carfixondemand.model.Car;
import com.gitlab.lauropereira.carfixondemand.model.Customer;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CarAddActivity extends AppCompatActivity {

    private static final String TAG = "CarAddActivity";
    private static final String KEY = "cars";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_add);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveCustomer();
                Toast.makeText(CarAddActivity.this, "Veículo salvo com sucesso", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.spinnersPopulate();

    }

    private boolean saveCustomer(){

        Car car = new Car();

        Spinner spTypes = (Spinner) findViewById(R.id.st_car_type);
        car.setType(spTypes.getSelectedItem().toString());

        Spinner spColors = (Spinner) findViewById(R.id.st_car_color);
        car.setColor(spColors.getSelectedItem().toString());

        Spinner spBrands = (Spinner) findViewById(R.id.st_car_brand);
        car.setBrand(spBrands.getSelectedItem().toString());

        Spinner spModels = (Spinner) findViewById(R.id.st_car_model);
        car.setModel(spModels.getSelectedItem().toString());

        EditText plate =  (EditText) findViewById(R.id.st_car_plate);
        car.setPlate(plate.getText().toString().toUpperCase().replaceAll("[^(\\d|[A-Z])]",""));

        Customer owner =  new Customer();
        EditText cpfOwner=  (EditText) findViewById(R.id.st_car_owner_cpf);
        owner.setCpf(cpfOwner.getText().toString().replaceAll("[^\\d]",""));
        car.setOwner(owner);

        DatabaseReference db = FirebaseDatabase.getInstance().getReference();
        db.child(KEY).child(car.getPlate()).setValue(car);

        return true;

    }

    private void spinnersPopulate() {
        Spinner spTypes = (Spinner) findViewById(R.id.st_car_type);
        Spinner spColors = (Spinner) findViewById(R.id.st_car_color);
        Spinner spBrands = (Spinner) findViewById(R.id.st_car_brand);
        Spinner spModels = (Spinner) findViewById(R.id.st_car_model);

        ArrayAdapter<CharSequence> typesAdapter = ArrayAdapter.createFromResource(this,
                R.array.car_types, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> colorsAdapter = ArrayAdapter.createFromResource(this,
                R.array.car_colors, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> brandAdapter = ArrayAdapter.createFromResource(this,
                R.array.car_brands, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> modelsAdapter = ArrayAdapter.createFromResource(this,
                R.array.car_model, android.R.layout.simple_spinner_item);

        typesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        colorsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        brandAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modelsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spTypes.setAdapter(typesAdapter);
        spColors.setAdapter(colorsAdapter);
        spBrands.setAdapter(brandAdapter);
        spModels.setAdapter(modelsAdapter);

        /** @TODO: Fazer spinner dinamico para Marcas dos veículos
        spTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        String uri = "http://fipeapi.appspot.com/api/1/" + parent.getSelectedItem().toString() + "/marcas.json";
        Log.i("CarTypeOnItemSelected", "Preparando para ler dados da API: " + uri);
        DownloadWebpageTask  tarefa = new DownloadWebpageTask();
        tarefa.execute(uri);
        }

        private void populateBrands(List<FipeBrandResult> itens) {
        List<String> brands = null;
        Log.i("CarTypeOnItemSelected", "Entrada do metodo de processamento!");
        for(FipeBrandResult item : itens ){
        Log.i("CarTypeOnItemSelected", "Item " + item.getName() + " incluido na lista.");
        brands.add(item.getName());
        }

        Log.i("CarTypeOnItemSelected", "tentando inserir os dados: " + brands.toString());

        ArrayAdapter<String> dataAdapter= new ArrayAdapter<String>(CarAddActivity.this, android.R.layout.simple_spinner_item, brands);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapter.notifyDataSetChanged();
        //spBrands.setAdapter(dataAdapter);
        }

        @Override
        public void onNothingSelected(AdapterView<?> view) {
        Toast.makeText(view.getContext(),
        "É necessário escolher 1 item!",
        Toast.LENGTH_LONG).show();
        }

        class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
        try {
        return WebServiceUtil.getContentAsString(urls[0]);
        } catch (IOException e) {
        Log.e("Exception", e.toString());//Observe que aqui uso o log.e e não log.d
        return "Problema ao montar a requisição";
        }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
        Log.d("teste", result);
        Gson g = new Gson();
        //FipeBrandResult retorno = g.fromJson(result.trim(), FipeBrandResult.class);
        Type listType = new TypeToken<ArrayList<FipeBrandResult>>(){}.getType();
        try {
        List<FipeBrandResult> retorno = new Gson().fromJson(result, listType);
        Log.i("CarTypeOnItemSelected", "Dados retornados da API: " + listType.toString());
        populateBrands(retorno);
        }catch (Exception exc){
        Log.e("Brands Spinner: ", "Falha ao processar retorno da tabela FIPE: " + exc.getMessage());
        }
        }
        }
        });
         */
    }
}
